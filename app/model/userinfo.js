module.exports = app => {
    const { STRING, TIME, INTEGER } = app.Sequelize;

    const Userinfo = app.model.define('userinfo', {
        unionId: {
            type: STRING,
            primaryKey: true
        },
        nickname: STRING,
        gender: INTEGER,
        vip_expire: TIME,
        area: STRING,
        avatar: STRING,
        match_code: STRING
    },
        {
            freezeTableName: true,
            timestamps: true,
            underscored: false
        });



    Userinfo.getUserNicknameByUnionId = async function (unionId) {
        return await this.findOne({
            raw: true,
            attributes:['nickname'],
            where: {
                unionId: unionId
            }
        });
    }

    
 
    return Userinfo;
};