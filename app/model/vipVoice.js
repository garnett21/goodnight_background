module.exports = app => {
    const { STRING, INTEGER } = app.Sequelize;
    const Op = app.Sequelize.Op


    const VipVoice = app.model.define('vip_voice', {
        voiceId: {
            type: INTEGER,
            primaryKey: true
        },
        audioId: STRING,
        duration: INTEGER,
        format: STRING,
        authorNickname: STRING
    }, {
            freezeTableName: true,
            timestamps: false
        });


        VipVoice.insertIntoVipVoice = async function (audioId, format) {
        return this.create({
            audioId: audioId,
            format: format,
        });
    };



    return VipVoice;
};