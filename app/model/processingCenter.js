module.exports = app => {
    const { STRING, INTEGER } = app.Sequelize;

    const ProcessingCenter = app.model.define('processing_center', {
        audioId: {
            type: STRING,
            primaryKey: true
        },
        tag: {
            type: INTEGER,
            primaryKey: true
        }
    }, {
            freezeTableName: true,
            timestamps: false
        });

    ProcessingCenter.findAudio = async function (audioId) {
        return await this.findAll({
            raw: true,
            where: {
                audioId: audioId
            }
        })
    };

    ProcessingCenter.addIntoProcessingCenter = async function (audioId, tag) {
        return await this.create({
            audioId: audioId,
            tag: tag
        });
    };


    ProcessingCenter.deleteFromProcessingCenter = async function (audioId, tag) {
        return await this.destroy({
            raw: true,
            where: {
                audioId: audioId,
                tag: tag
            }
        });
    };

    ProcessingCenter.deleteAudiosForPushing = async function (audioId) {
        return await this.destroy({
            raw: true,
            where: {
                audioId: audioId
            }
        });
    };


    ProcessingCenter.updateProcessingCenter = async function (audioId, tags) {
        return await this.update(
            pram = { 'tag': tags }, {
                raw: true,
                where: {
                    audioId: audioId,
                    tag: tags
                }
            });
    };


    ProcessingCenter.findAudiosByTag = async function (tag) {
        return this.findAll({
            raw: true,
            where: {
                tag: tag
            }
        });
    };

    ProcessingCenter.findTagsByAudioId = async function (audioId) {
        return this.findAll({
            raw: true,
            attributes: ['tag'],
            where: {
                audioId: audioId
            }
        });
    };


    return ProcessingCenter;
}