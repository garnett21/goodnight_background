module.exports = app => {
    const { STRING, TIME, INTEGER } = app.Sequelize;
    const AddPushDateToAudio = app.model.define('add_pushDate_to_audios', {
        audioId: {
            type: STRING,
            primaryKey: true
        },
        pushDate: TIME,
    }, {
            freezeTableName: true,
            timestamps: false
        })


    AddPushDateToAudio.addPushDateToAudio = async function (audioId, pushDate) {
        return await this.create({
            audioId: audioId,
            pushDate: pushDate,
        });
    };


    AddPushDateToAudio.updatePushDate = async function (audioId, pushDate) {
        return await this.update(
            { 'pushDate': pushDate }, {
                raw: true,
                where: {
                    audioId: audioId
                }
            })
};


    AddPushDateToAudio.IsPushDateExist = async function (pushDate) {
        return await this.findOne({
            raw: true,
            where: {
                pushDate: pushDate
            }
        });
    };


    AddPushDateToAudio.listAllAudios = async function () {
        return await this.findAll({
            raw: true
        });
    };

    AddPushDateToAudio.findPushDateByAudioId = async function (audioId) {
        return await this.findOne({
            raw: true,
            where: {
                audioId: audioId
            }
        });
    };







    return AddPushDateToAudio;
};