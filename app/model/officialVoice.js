module.exports = app => {
    const { STRING, DATE, INTEGER } = app.Sequelize;

    const OfficialVoice = app.model.define('official_voice', {
        audioId: {
            type: INTEGER,
            primaryKey: true
        },
        pushTime: DATE,
        duration: INTEGER,
        format: STRING,
        authorNickname: STRING
    }, {
            freezeTableName: true,
            timestamps: false
        }
    );

   

    OfficialVoice.insertIntoOfficialVoice = async function(audioId,pushTime,format,duration,remark){
        return await this.create({
            audioId:audioId,
            pushTime:pushTime,
            format:format,
            duration:duration,
            authorNickname:remark

        });
    };




    return OfficialVoice;
};