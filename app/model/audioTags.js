module.exports = app =>{
    const {INTEGER,STRING} = app.Sequelize;

    const AudioTags = app.model.define('audio_tags',{
        tagId:{
            type: INTEGER,
            primaryKey: true
        },
        tag: STRING
    },    
    {
        freezeTableName: true,
        timestamps: false,
    });

    AudioTags.showTags = async function(){
        return await this.findAll({
            raw:true
        });
    };
    
    AudioTags.addATag = async function(tagName){
        return await this.create({
            tag:tagName,
        });
    };

    AudioTags.deleteAtag = async function(tagName){
        return await this.destroy({
            raw:true,
            where:{
                tag:tagName,
            }
        });
    };

    return AudioTags;

};