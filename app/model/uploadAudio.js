module.exports = app => {
    const { STRING, TIME, INTEGER } = app.Sequelize;

    const UploadAudio = app.model.define('upload_audio', {
        audioId: {
            type: STRING,
            primaryKey: true
        },
        unionId: STRING,
        duration: INTEGER,
        createdAt: TIME,
    },
        {
            freezeTableName: true,
            timestamps: true,
            underscored: false,
            updatedAt: false,

        });

        
    UploadAudio.getNewestAudio = async function(last,now){
        return await this.findAll({
            raw:true,
            where:{
                createdAt:{
                    $between:[last,now]
                },
            }
        })
    };


    UploadAudio.getAuthorUnionIdAndDurationByAudioId = async function(audioId){
        return await this.findOne({
            raw:true,
            attributes:['unionId','duration'],
            where :{
                audioId:audioId
            }
        })
      }


    return UploadAudio;
};