module.exports = app => {
    const { STRING } = app.Sequelize;

    const Admin = app.model.define('administrator',{
        account:{
            type:STRING,
            primaryKey:true
        },
        password:STRING,
        realName:STRING
    },{ freezeTableName: true,
        timestamps: false }
    )

    Admin.register = async function(realName,account,hashPassword){
     return await this.create({
         realName:realName,
         account:account,
         password:hashPassword
     })
    };

    Admin.login = async function(account){
        return await this.findOne({
            raw:true,
            attributes:['password'],
            where:{
                account:account
            }
        })
    };
    return Admin;
};