module.exports = app => {
    const { STRING } = app.Sequelize;
    const AddRemarkToAudio = app.model.define('add_remark_to_audios', {
        audioId: {
            type: STRING,
            primaryKey: true
        },
        remark: STRING,
    }, {
            freezeTableName: true,
            timestamps: false
        })

        AddRemarkToAudio.addRemarkToAudio = async function(audioId, remark){
        return await this.create({
            audioId: audioId,
            remark: remark,
        });
    }


    AddRemarkToAudio.updateRemark = async function(audioId, remark){
        return await this.update({
            'remark':remark
        },{
            raw:true,
            where:{
                audioId: audioId,
            }
        });
    }

    AddRemarkToAudio.findRemarkByAudioId = async function(audioId){
        return await this.findOne({
            raw:true,
            where:{
                audioId:audioId
            }
        });
    };


    return AddRemarkToAudio;
};