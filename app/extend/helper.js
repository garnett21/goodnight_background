module.exports = {
   

    dateFormater(dateObj) {
        return `${dateObj.getFullYear()}-${dateObj.getMonth() + 1}-${dateObj.getDate()}`;
    },


    getYesterday() {
        const dayMilliseconds = 86400000;
        let yesterday = new Date(new Date().getTime() - dayMilliseconds);
        return yesterday;
    },

    getTomorrow() {
        const dayMilliseconds = 86400000;
        let tomorrow = new Date(new Date().getTime() + dayMilliseconds);
        return tomorrow;
    },

    getToday() {
        let today = new Date(new Date().getTime());
        return today;
    }

};
