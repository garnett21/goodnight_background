/**
 * 用户模块错误 01
 */

 module.exports = {
     PASSWORD_DO_NOT_MATCH:{
         errorCode:1010001,
         status:401,
         message:'password do not match'
     },

     USER_NOT_FOUND:{
        errorCode:1010002,
        status:404,
        message:'user not found'
    }
 };