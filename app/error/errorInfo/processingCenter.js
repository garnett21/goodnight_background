/**
 * 语音处理模块错误 02
 */

module.exports = {
    THSES_TWO_TAGS_CAN_NOT_EXIST_AT_THE_SANME_TIME:{
        errorCode:2010001,
        status:400,
        message:'these two tags can not exist at the same time'
    },

    CAN_NOT_DELETE_DEFAULT_TAG:{
        errorCode:2010002,
        status:403,
        message:'can not delete default tag'
    },

    CAN_NOT_INSERT_THE_SAME_PUSHDATE:{
        errorCode:2010003,
        status:403,
        message:'can not insert the same pushdate'
    }
};