'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.post('/user/login', controller.admin.login);
  router.post('/user/register', controller.admin.register);

  router.get('/processingCenter/listAudio',app.jwt, controller.processingCenter.listAudio);
  router.post('/processingCenter/tagForAudio',app.jwt, controller.processingCenter.tagForAudio);
  router.get('/processingCenter/getTags',app.jwt, controller.processingCenter.getTags);
  router.post('/processingCenter/addATag',app.jwt, controller.processingCenter.addATag);
  router.put('/processingCenter/deleteATag',app.jwt, controller.processingCenter.deleteATag);
  router.post('/processingCenter/addRemarkToAudio',app.jwt, controller.processingCenter.addRemarkToAudio);
  router.post('/processingCenter/addPushDateToAudio',app.jwt, controller.processingCenter.addPushDateToAudio);
  router.get('/processingCenter/secondProcessingCenter',app.jwt, controller.processingCenter.secondProcessingCenter);
  router.post('/processingCenter/pushAudioToOfficialTable',app.jwt, controller.processingCenter.pushAudioToOfficialTable);
  // router.post('/processingCenter/pushAudiosToOfficialVoice',app.jwt, controller.processingCenter.pushAudiosToOfficialVoice);
  // router.get('/processingCenter/pushAllAudiosBeTagAsVIP',app.jwt, controller.processingCenter.pushAllAudiosBeTagAsVIP);

};
