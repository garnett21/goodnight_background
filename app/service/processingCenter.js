const Service = require('egg').Service;
const ProcessingCenterErrorInfo = require('../error/errorInfo/processingCenter');
const HttpCustomError = require('../error/HttpCustomError');

class processingCenterService extends Service {
  async listAudio() {
    let last = await this.app.redis.get(`lastTimeRequestForTheNewestAudio`);
    let lastTime = new Date(last);
    let currentTime = this.ctx.helper.getToday();
    await this.app.redis.set(`lastTimeRequestForTheNewestAudio`, currentTime);
    return await this.ctx.model.UploadAudio.getNewestAudio(lastTime, currentTime);
  };


  async getTags() {
    let tags = await this.ctx.model.AudioTags.showTags();
    console.log(tags);
    return tags;
  };

  async addATag(tagName) {
    await this.ctx.model.AudioTags.addATag(tagName);
  };


  async deleteATag(tagName) {
    if (tagName != '1' && tagName != '2' && tagName != '3' && tagName != '4') {
      await this.ctx.model.AudioTags.deleteATag(tagName);
    } else {
      throw new HttpCustomError(ProcessingCenterErrorInfo.CAN_NOT_DELETE_DEFAULT_TAG);
    };
  };


  async addRemarkToAudio(audioId, remark) {
    let isRecordExist = await this.ctx.model.AddRemarkToAudio.findRemarkByAudioId(audioId);
    if (isRecordExist == null) {
      await this.ctx.model.AddRemarkToAudio.addRemarkToAudio(audioId, remark);
    } else {
      await this.ctx.model.AddRemarkToAudio.updateRemark(audioId, remark)
    };

  };

  async addPushDateToAudio(audioId, pushDate) {
    let isPushDateExist = await this.ctx.model.AddPushDateToAudio.IsPushDateExist(pushDate);
    let isRecordExist = await this.ctx.model.AddPushDateToAudio.findPushDateByAudioId(audioId);
    console.log(isPushDateExist);
    if (isPushDateExist == null && isRecordExist == null) {
      await this.ctx.model.AddPushDateToAudio.addPushDateToAudio(audioId, pushDate);
    } else {
      if (isPushDateExist == null && isRecordExist != null) {
        await this.ctx.model.AddPushDateToAudio.updatePushDate(audioId, pushDate);
      } else {
        throw new HttpCustomError(ProcessingCenterErrorInfo.CAN_NOT_INSERT_THE_SAME_PUSHDATE);
      }
    };
  };

  async saveIntoProcessingCenter(newestAudio) {
    for (var i = 0; i < newestAudio.length; i++) {
      let audioId = newestAudio[i].audioId;
      await this.ctx.model.ProcessingCenter.addIntoProcessingCenter(audioId);
    };
  };

  async tagForAudio() {
    let tags = new Array();
    let tag = this.ctx.request.body.tag;
    let audioId = this.ctx.request.body.audioId;
    for (var i = 0; i < tag.length; i++) {
      tags.push(tag[i]);
    };
    let records = await this.ctx.model.ProcessingCenter.findAudio(audioId);
    if (records != null) {
      for (var i = 0; i < records.length; i++) {
        let tag = records[i].tag;
        await this.ctx.model.ProcessingCenter.deleteFromProcessingCenter(audioId, tag);
      };
    };

    if (tags.indexOf('4') != -1 && tags.indexOf('3') != -1) {
      throw new HttpCustomError(ProcessingCenterErrorInfo.THSES_TWO_TAGS_CAN_NOT_EXIST_AT_THE_SANME_TIME);
    } else {
      if (tags.indexOf('4') != -1) {
        tags.splice(0, tags.length);
        tags.push('4');
        await this.ctx.model.ProcessingCenter.addIntoProcessingCenter(audioId, tags);
      } else {
        if (tags.indexOf('3') != -1) {
          tags.splice(0, tags.length);
          tags.push('3');
          console.log(tags);
          await this.ctx.model.ProcessingCenter.addIntoProcessingCenter(audioId, tags);
        } else {
          for (var i = 0; i < tags.length; i++) {
            console.log(tags[i]);
            await this.ctx.model.ProcessingCenter.addIntoProcessingCenter(audioId, tags[i]);
          };
        };
      };
    };
  };

  async secondProcessingCenter(tag) {
    let audioMessgaes = new Array();
    let secondProcessingCenter = await this.ctx.model.ProcessingCenter.findAudiosByTag(tag);
    console.log(secondProcessingCenter);
    for (var i = 0; i < secondProcessingCenter.length; i++) {
      let tags = new Array();
      let audioId = secondProcessingCenter[i].audioId;

      let findPushDateByAudioId = await this.ctx.model.AddPushDateToAudio.findPushDateByAudioId(audioId);
      let pushDate;
      if (findPushDateByAudioId != null) {
        pushDate = findPushDateByAudioId.pushDate;
      } else {
        pushDate = null;
      };

      let findRemarkByAudioId = await this.ctx.model.AddRemarkToAudio.findRemarkByAudioId(audioId);
      let remark;
      if (findRemarkByAudioId != null) {
        remark = findRemarkByAudioId.remark;
      } else {
        remark = null;
      };

      let findTagsByAudioId = await this.ctx.model.ProcessingCenter.findTagsByAudioId(audioId);
      for (var j = 0; j < findTagsByAudioId.length; j++) {
        let tag = findTagsByAudioId[j].tag;
        tags.push(tag);
      };
      JSON.stringify(tags);
      let authorUnionIdAndDuration = await this.ctx.model.UploadAudio.getAuthorUnionIdAndDurationByAudioId(audioId);
      let unionId = authorUnionIdAndDuration.unionId;
      let duration = authorUnionIdAndDuration.duration;
      let nickname = await this.ctx.model.Userinfo.getUserNicknameByUnionId(unionId);
      let auduomessage = { 'audioId': audioId, 'nickname': nickname, 'duration': duration, 'pushDate': pushDate, 'tags': tags, 'remark': remark };
      audioMessgaes.push(auduomessage);
    };
    return audioMessgaes;
  };


  async pushAudioToOfficialTable(tag) {
    let audiosForPushing = await this.ctx.model.ProcessingCenter.findAudiosByTag(tag);
    console.log(tag);
    switch (tag) {
      case '1':
      console.log('=========');
        for (var i = 0; i < audiosForPushing.length; i++) {
          let tags = new Array();
          let audioId = audiosForPushing[i].audioId;

          let findPushDateByAudioId = await this.ctx.model.AddPushDateToAudio.findPushDateByAudioId(audioId);
          let pushDate;
          if (findPushDateByAudioId != null) {
            pushDate = findPushDateByAudioId.pushDate;
          } else {
            pushDate = null;
          };

          let findRemarkByAudioId = await this.ctx.model.AddRemarkToAudio.findRemarkByAudioId(audioId);
          let remark;
          if (findRemarkByAudioId != null) {
            remark = findRemarkByAudioId.remark;
          } else {
            remark = null;
          };

          let findTagsByAudioId = await this.ctx.model.ProcessingCenter.findTagsByAudioId(audioId);
          for (var j = 0; j < findTagsByAudioId.length; j++) {
            let tag = findTagsByAudioId[j].tag;
            tags.push(tag);
          };
          JSON.stringify(tags);
          let authorUnionIdAndDuration = await this.ctx.model.UploadAudio.getAuthorUnionIdAndDurationByAudioId(audioId);
          let duration = authorUnionIdAndDuration.duration;
          let format = 'm4a';
          await this.ctx.model.OfficialVoice.insertIntoOfficialVoice(audioId, pushDate, format, duration, remark);
          await this.ctx.model.ProcessingCenter.deleteAudiosForPushing(audioId);
        };
        break;
      case '2':
        for (var i = 0; i < audiosForPushing.length; i++) {
          let audioId = audiosForPushing[i].audioId;
          let format = 'm4a';
          await this.ctx.model.VipVoice.insertIntoVipVoice(audioId, format);
          await this.ctx.model.ProcessingCenter.deleteAudiosForPushing(audioId);
        };
        break;
    };
  };





  // async pushAudiosToOfficialVoice() {
  //   let audios = await this.ctx.model.AddPushDateToAudios.listAllAudios();
  //   console.log(audios);
  //   for (var i = 0; i < audios.length; i++) {
  //     let audioId = audios[i].audioId;
  //     let pushTime = audios[i].pushDate;
  //     let remark = audios[i].remark;
  //     let authorUnionIdAndDuration = await this.ctx.model.UploadAudio.getAuthorUnionIdAndDurationByAudioId(audioId);
  //     let duration = authorUnionIdAndDuration.duration;
  //     let format = 'm4a';
  //     await this.ctx.model.OfficialVoice.insertIntoOfficialVoice(audioId, pushTime, format, duration, remark);
  //   };

  // };

  // async pushAllAudiosBeTagAsVIP() {
  //   let VIPAudios = await this.ctx.model.ProcessingCenter.findAudiosByTag('1');
  //   console.log(VIPAudios);
  //   for (var i = 0; i < VIPAudios.length; i++) {
  //     let audioId = VIPAudios[i].audioId;
  //     let format = 'm4a';
  //     await this.ctx.model.VipVoice.insertIntoVipVoice(audioId, format);
  //     await this.sendPrivateMessageToUser(audioId);
  //   };
  // };


  async sendPrivateMessageToUser(audioId) {
    let authorUnionIdAndDuration = await this.ctx.model.UploadAudio.getAuthorUnionIdAndDurationByAudioId(audioId);
    let unionId = authorUnionIdAndDuration.unionId;
    let duration = authorUnionIdAndDuration.duration;


  };


};
module.exports = processingCenterService;