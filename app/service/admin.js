const Service = require('egg').Service;
const bcrypt = require('bcrypt');
const saltRounds = 10;
const UserErrorInfo = require('../error/errorInfo/user');
const HttpCustomError = require('../error/HttpCustomError');

class adminService extends Service {
    async register(realName, account, password) {
        console.log(password);
        let hashPassword = await bcrypt.hash(password, saltRounds);
        console.log(hashPassword);
        await this.ctx.model.Admin.register(realName, account, hashPassword);
    };

    async login(account, password) {
        let searchForPassword = await this.ctx.model.Admin.login(account);
        if (searchForPassword == null) {
            throw new HttpCustomError(UserErrorInfo.USER_NOT_FOUND);
        } else {
            let hashPassword = searchForPassword.password;
            let comparison = await bcrypt.compare(password,hashPassword);
            if(comparison == true){
                let token = this.signToken(account, password);
                return token;        
            }else{
                throw new HttpCustomError(UserErrorInfo.PASSWORD_DO_NOT_MATCH);
            };
        };
    };


    signToken(account, password) {
        let token = this.app.jwt.sign(
            {
                account: account,
                password: password
            },
            this.app.config.jwt.secret,
            this.app.config.jwt.option
        );
        return token;
    };


}

module.exports = adminService;