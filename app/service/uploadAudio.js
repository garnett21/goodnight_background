const Service = require('egg').Service;
const UserErrorInfo = require('../error/errorInfo/user');
const HttpCustomError = require('../error/HttpCustomError');

class uploadAudioService extends Service {
  async listAudio(){
    let last = await this.app.redis.get(`lastTimeRequestForTheNewestAudio`);
    let lastTime = new Date(last);
    let currentTime = this.ctx.helper.getToday();
    await this.app.redis.set(`lastTimeRequestForTheNewestAudio`,currentTime);
    return await this.ctx.model.UploadAudio.getNewestAudio(lastTime,currentTime);
  };

  async saveIntoProcessingCenter(newestAudio){
    for(var i = 0; i < newestAudio.length; i++){
        let audioId = newestAudio[i].audioId;
        let unionId = newestAudio[i].unionId;
        let duration = newestAudio[i].duration;
        await this.ctx.model.ProcessingCenter.addIntoProcessingCenter(audioId,unionId,duration);
    };
};
};
module.exports = uploadAudioService;