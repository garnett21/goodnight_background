'use strict';
const Controller = require('egg').Controller;

class BaseController extends Controller {
    success(data, message, code, statusCode) {
        this.ctx.body = {
            success: true,
            data: data,
            message: message,
            code: code,
        }
        this.ctx.status = statusCode || 200;
    }

    fail(message, errorCode, statusCode) {
        this.ctx.body = {
            success: false,
            message: message,
            errorCode: errorCode
        }
        this.ctx.status = statusCode || 400;
    }

   
}

module.exports = BaseController;
