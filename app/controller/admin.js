'use strict';
const BaseController = require('./base');

class adminController extends  BaseController{
    async register(){
        let realName = this.ctx.request.body.realName;
        let account = this.ctx.request.body.account;
        let password = this.ctx.request.body.password;
        await this.service.admin.register(realName,account,password);
        
    };
    async login(){
        let account = this.ctx.request.body.account;
        let password = this.ctx.request.body.password;
        let token = await this.service.admin.login(account,password);
        this.success({token},'successfully login',200);
    };

    async logout(){
        this.success('token correct',200);
    };


};
module.exports = adminController;