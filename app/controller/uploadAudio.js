'use strict';
const BaseController = require('./base');
let OSS = require('ali-oss');

class uploadAudioController extends BaseController {
    async listAudio() { 
        let newestAudio =  await this.service.uploadAudio.listAudio();
        await this.saveIntoProcessingCenter(newestAudio);
        this.success(newestAudio,'these are the newest audio',200);
    };


    async saveIntoProcessingCenter(newestAudio){
         await this.service.uploadAudio.saveIntoProcessingCenter(newestAudio);
    };















        // let client = new OSS({
        //     region: this.app.config.alioss.region,
        //     accessKeyId: this.app.config.alioss.accessKeyId,
        //     accessKeySecret: this.app.config.alioss.accessKeySecret,
        //     bucket: this.app.config.alioss.bucket
        // });
        // client.useBucket(this.app.config.alioss.bucket);
        // let result = await client.list({
        //     'prefix':'user_upload_audio',
        //     'max-keys': 5
        // })
        // console.log(result);


};
module.exports = uploadAudioController;