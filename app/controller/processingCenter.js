'use strict';
const BaseController = require('./base');
let OSS = require('ali-oss');

class processingCenterController extends BaseController {
    async listAudio() {
        let newestAudio = await this.service.processingCenter.listAudio();
        await this.saveIntoProcessingCenter(newestAudio);
        this.success(newestAudio, 'these are the newest audio', 200);
    };


    async saveIntoProcessingCenter(newestAudio) {
        await this.service.processingCenter.saveIntoProcessingCenter(newestAudio);
    };

    async getTags() {
        let tags = await this.service.processingCenter.getTags();
        this.success(tags, 'these are the tags you set', 200);
    };

    async addATag() {
        let tagName = this.ctx.request.body.tagName;
        await this.service.processingCenter.addATag(tagName);
    };

    async deleteATag() {
        let tagName = this.ctx.request.body.tagName;
        await this.service.processingCenter.deleteATag(tagName);
    };

    async addRemarkToAudio() {
        let audioId = this.ctx.request.body.audioId;
        let remark = this.ctx.request.body.remark;
        await this.service.processingCenter.addRemarkToAudio(audioId, remark);
        this.success('successfully add reamrk to a aduio', 200);
    };

    async tagForAudio() {
        await this.service.processingCenter.tagForAudio();
        this.success('successfully tag for audio', 200);
    };

    async addPushDateToAudio() {
        let audioId = this.ctx.request.body.audioId;
        let pushDate = this.ctx.request.body.pushDate;
        await this.service.processingCenter.addPushDateToAudio(audioId, pushDate);
        this.success('successfully add pushDate', 200);
    };

    async secondProcessingCenter(){
        let tag = this.ctx.query.tag;
        console.log(tag);
        let secondProcessingCenter = await this.service.processingCenter.secondProcessingCenter(tag);
        this.success(secondProcessingCenter,'this is the second processing',200);
    };

    async pushAudioToOfficialTable() {
        let tag = this.ctx.request.body.tag;
        await this.service.processingCenter.pushAudioToOfficialTable(tag);
        this.success('successfully push to official table',200);
    };




















    // let client = new OSS({
    //     region: this.app.config.alioss.region,
    //     accessKeyId: this.app.config.alioss.accessKeyId,
    //     accessKeySecret: this.app.config.alioss.accessKeySecret,
    //     bucket: this.app.config.alioss.bucket
    // });
    // client.useBucket(this.app.config.alioss.bucket);
    // let result = await client.list({
    //     'prefix':'user_upload_audio',
    //     'max-keys': 5
    // })
    // console.log(result);


};
module.exports = processingCenterController;