/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {
    security: {
      csrf: {
        enable: false,
      },
  }
};


config.sequelize = {
  dialect: 'mysql', // support: mysql, mariadb, postgres, mssql
  database: 'wyn_goodnight',
  host: 'localhost',
  port: '3306',
  username: 'root',
  password: '123456',
  dialectOptions: {
    useUTC: false, //for reading from database
    dateStrings: true,

    typeCast: function (field, next) { // for reading from database
      if (field.type === 'DATETIME') {
        return field.string()
      }
      return next()
    },
  },
  timezone: '+08:00'
}

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1550717538180_9839';

  // add your middleware config here
  config.middleware = [];

  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };

  config.alioss = {
    accessKeyId: 'LTAIX9AQ6J5XlQNV',
    accessKeySecret: '0XXjtUwuatQdNNMcl9nWhYxhJgSq2X',
    bucket: 'wyn-goodnight',
    region: 'oss-cn-shenzhen'
  }



  return {
    ...config,
    ...userConfig,
  };
};
