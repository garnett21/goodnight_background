'use strict';

/** @type Egg.EggPlugin */
exports.jwt = {
  enable: true,
  package: "egg-jwt"
};

exports.sequelize = {
  enable: true,
  package: 'egg-sequelize'
};

exports.redis = {
  enable: true,
  package: 'egg-redis',
};
